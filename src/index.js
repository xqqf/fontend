import React from 'react';
import { render } from 'react-dom';
import ReactDom from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import NormalLoginForm from "./component/login";
import Reg from "./component/reg";
import Pub from "./component/pub"
import L from "./component/list"
import { Layout, Menu, Icon, LocaleProvider } from 'antd';

import 'antd/lib/menu/style';
import 'antd/lib/icon/style';
import 'antd/lib/layout/style';
import "./css/login.css";
import "./css/reg.css";
import zhCN from 'antd/lib/locale-provider/zh_CN'

const { Header, Content, Footer } = Layout;


const Home = () => (
  <div><h2>Home2...</h2></div>
);

const About = () => (
  <div><h2>About2++++</h2></div>
);


class Root extends React.Component {
  render() {
    return (

      <Router>
        <Layout className="layout">
          <Header>

            <Menu mode="horizontal" theme="dark">
              <Menu.Item key="home">
                <Link to="/">主页</Link>
              </Menu.Item>
              <Menu.Item key="login">
                <Link to="/login">登录</Link>
              </Menu.Item>
              <Menu.Item key="reg">
                <Link to="/reg">注册</Link>
              </Menu.Item>
              <Menu.Item key="pub">
                <Link to="/pub">发布</Link>
              </Menu.Item>
              <Menu.Item key="lsit">
                <Link to="/list">文章列表</Link>
              </Menu.Item>
              <Menu.Item key="about">
                <Link to="/about">关于</Link>
              </Menu.Item>
            </Menu>

          </Header>
          <Content style={{ padding: '8px 50px' }}>
            <div style={{ background: '#fff', padding: 24, minHeight: 200 }}>

              <Route exact path="/" component={Home} />
              <Route path="/login" component={NormalLoginForm} />
              <Route path="/reg" component={Reg} />
              <Route path="/pub" component={Pub} />
              <Route path="/list" component={L} />
              <Route path="/about" component={About} />
            </div>
          </ Content>
          <Footer style={{ textAlign: 'center' }}>
            yang blog ©2019 Created by yang.
          </Footer>
        </Layout>
      </Router>
    );
  }
}

// ReactDom.render(<App />, document.getElementById("root"));
ReactDom.render(
  <LocaleProvider locale={zhCN}>
      <Root />
  </LocaleProvider>, document.getElementById("root"));

