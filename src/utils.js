import React from 'react';

const injects = obj => Comp => props => < Comp {...obj} {...props}/>

/**
 * 函数功能是什么？解析qs
 * 输入url
 * 输出对象
 *  */

function parse_qs(search) {
    var re =/(\w+)=([^&]+)/; 
    if (search[0] == "?")
       search = search.substr(1)
   var ret = {};
   // console.log(search.split("&"))
   search.split("&").forEach(element => {
       match = re.exec(element);
       if (match) {
           // console.log(match);
           ret[match[1]] = match[2];
           }
       
       })
   return ret
   }

export {injects, parse_qs};