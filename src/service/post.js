
import axios from 'axios';
import {observable} from 'mobx';
import store from 'store';




export default class PostService {

    constructor(){
        this.instance = axios.create({
            baseURL: "/api/post/"
        });
    }


    @observable done = false;
    @observable errMsg = "";
    @observable posts = [];
    @observable pagination = {page:1, size:5, pages:1, count:0}
    pub (title, content) {
        console.log(1, title, content);
        
        this.instance.post("pub", {
            title, content
        }, {
            headers: {'JWT': store.get('token')}
        })
        .then(response => {
            this.errMsg = "文章提交成功"
            this.done = true
        }).catch(error => {
            console.log(error);
            this.errMsg = "文章提交错误"
        })
    }


    getall(search) {
        this.instance.get(search 
        ).then(response => {
            console.log(1, response.data.posts)
            this.posts = response.data.posts;
            console.log(2,response.data.pagination)
            this.pagination = response.data.pagination;
        }).catch(error => {
            console.log(error);
            this.errMsg = "文章提交错误"
        })
    }

}