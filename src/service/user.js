
import axios from 'axios';
import store from 'store';
import { changeConfirmLocale } from 'antd/lib/modal/locale';
import {observable, isBoxedObservable} from 'mobx';
import {observer} from 'mobx-react';


store.addPlugin(require('store/plugins/expire'));
// store.set('token', res.data.token, (new Date().getTime() + (8 * 3600 * 1000)))

export default class UserService {
    @observable Loggedin = false;
    @observable regged = "";
    @observable errMsg = "";


    login (email, password) {
        console.log(1, email, password);

        axios.post('/api/user/login', {
            email:email,
            password:password
        })
        .then(response => {
            store.set('token', response.data.token, (new Date().getTime() + (8 * 3600 * 1000)))
            
            this.Loggedin = true
        }).catch(error => {
            console.log(error);
            this.errMsg = "登陆失败,账号或密码错误."
        })
    }


    reg (email, password, name) {
        console.log(1, email, password, name);

        axios.post('/api/user/reg', {
            email, password, name})
        .then(response => {
            store.set('token', response.data.token, (new Date().getTime() + (8 * 3600 * 1000)))

            this.regged = "注册成功"

        }).catch(error => {
            console.log(error);
            this.errMsg = "注册失败,该用户已注册."
        })
    }
}