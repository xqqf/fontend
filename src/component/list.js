import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { message, List, Pagination } from 'antd';

import PostService from '../service/post';
import { injects } from "../utils";

import "antd/lib/message/style";
import "antd/lib/list/style";

const service = new PostService();

@injects({ service })
@observer
export default class L extends React.Component {
    constructor(props) {
        super(props);
        console.log(props)
        const { location: { search } } = props;
        console.log(search)
        // props.service.list(props.location.search)
        props.service.getall(search)

    }
    handleChange(pageNo, pageSize) {
        const search = "?page=" + pageNo + "&size=" + pageSize
        this.props.service.getall(search)
    }

    render() {
        const data = this.props.service.posts;
        const pagination = this.props.service.pagination;
        if (data.length) {
            return (
                <List bordered={true} dataSource={data} renderItem={
                item => (<List.Item><Link to={'/post/'+item.post_id}>{item.title}</Link></List.Item>)}
                pagination={
                    {
                        onChange:this.handleChange.bind(this),
                        pagesize:pagination.szie,
                        total:pagination.count,
                        current:pagination.page,
                    }
                }
                    
                />);
        } else {
            return (<div></div>)
        }
    }
}