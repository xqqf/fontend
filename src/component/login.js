import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Button } from 'antd/lib/radio';
import { observer, inject } from 'mobx-react';
import UserService from '../service/user';
import { message } from 'antd';
import "antd/lib/message/style";

import { injects } from "../utils"
import "../css/login.css";



const service = new UserService();


@injects({ service })
@observer
export default class Login extends React.Component {

    handleCheck(event) {
        event.preventDefault();
        let fm = event.target.form
        this.props.service.login(fm[0].value, fm[1].value, this);
    }

    render() {
        console.log(this.props.service.Loggedin, '+++++++++++++++++++++++')
        if (this.props.service.Loggedin) {
            console.log(this.props.service.Loggedin);
            return (
                <Redirect to='/about' />
            );
        };
        if (this.props.service.errMsg) {
            console.log(message)
            // alert(this.props.service.errMsg)
            message.info(this.props.service.errMsg, 3,
                () => setTimeout(() => this.props.service.errMsg = ""), 1000);
        }

        return (<div className="login-page">
            <div className="form">
                <form className="login-form">
                    <input type="text" placeholder="邮箱" value="tom1@wsecar.com" />
                    <input type="password" placeholder="密码" />
                    <button onClick={this.handleCheck.bind(this)}>登录</button>
                    <p className="message">还未注册？<Link to="/reg">请注册.</Link></p>
                </form>
            </div>Í
        </div>);
    };
}



