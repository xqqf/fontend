import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { message, Form, Input,Button } from 'antd';

import PostService from '../service/post';
import { injects } from "../utils";

import "antd/lib/button/style";
import "antd/lib/message/style";
import "antd/lib/form/style";
import "antd/lib/input/style";

const { TextArea } = Input;
const FormItem = Form.Item;
const service = new PostService();

@injects({ service })
@observer
export default class Pub extends React.Component {
    handleSubmit(event) {
        event.preventDefault();
        console.log(event.target)
        let fm = event.target
        console.log(fm[0].value, fm[1].value)
        this.props.service.pub(fm[0].value, fm[1].value);
    }

    render() {
        if (this.props.service.errMsg) {
            message.info(this.props.service.errMsg, 3,
            () => setTimeout(() => this.props.service.errMsg=""), 1000)
        };
        // 表单，antd
        return (
            <Form layout="vertical" onSubmit={this.handleSubmit.bind(this)}>
                <FormItem label="标题" labelCol={{span: 4}} wrapperCol={{span: 14}}>
                    <Input />
                </FormItem>
                <FormItem label="正文" labelCol={{span: 4}} wrapperCol={{span: 14}}>
                    <TextArea rows={30} />
                </FormItem>
                <FormItem wrapperCol={{span: 14, offset: 4}}>
                    <Button type="primary" htmlType="submit">提交</Button>
                </FormItem>
            </Form>
        )
    }
}