import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Button } from 'antd/lib/radio';
import "../css/reg.css";
import UserService from '../service/user';
import { observer, inject } from 'mobx-react';
import { message } from 'antd';
import "antd/lib/message/style";
import { injects } from "../utils"
const service = new UserService();


@injects({ service })
@observer
export default class Reg extends React.Component {
    handleClick(event) {
        event.preventDefault();
        let fm = event.target.form;
        this.props.service.reg(fm[1].value, fm[2].value, fm[0].value);
    }
    render() {
        console.log(this.props.service.regged, '+++++++++++++++++++++++')
        if (this.props.service.regged) {
            console.log(this.props.service.regged);
            message.info(this.props.service.regged, 3,
                setTimeout(() => this.props.service.regged = ""), 1000);
            return (
                <Redirect to='/about' />
            );
        };
        if (this.props.service.errMsg) {
            console.log(message)
            // alert(this.props.service.errMsg)
            message.info(this.props.service.errMsg, 3,
                () => setTimeout(() => this.props.service.errMsg = ""), 1000);
        }
        return (

            <div className="reg-page">
                <div className="form">
                    <form className="reg-form">
                        <input type="text" placeholder="用户名" />
                        <input type="text" placeholder="邮箱" />
                        <input type="password" placeholder="密码" />
                        <input type="password" placeholder="确认密码" />
                        <button onClick={this.handleClick.bind(this)}>注册</button>
                        <p className="message">已注册?<Link to="/login"> 请登录.</Link></p>
                    </form>
                </div>Í
            </div>);


    }

}